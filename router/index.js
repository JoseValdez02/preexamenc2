import express  from 'express';
import  json  from 'body-parser';

export const router = express.Router();



// Ruta para el Pago de Recibo
router.get('/', (req, res) => {
    const params = {
        numRecibo: req.query.numRecibo,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        servicio: req.query.servicio,
        kwConsumidos: req.query.kwConsumidos,
        isPost: false,
    };
    res.render('index', params);    
});

// Ruta para procesar la tabla de Pago de Recibo
router.post('/', (req, res) => {
    const precios = [1.08, 2.5, 3.0];

    const { numRecibo, nombre, domicilio, servicio, kWCon } = req.body;
    const costKw = precios[servicio * 1];
    const tipoServicio = servicio == 0 ? 'Domestico' :  servicio == 1 ? 'Comercial' : 'Industrial'
    const subtotal = costKw * kWCon;

    // Calcular el descuento
    let descuento = 0;
    if (kWCon <= 1000) {
        descuento = 0.1;
    } else if (kWCon > 1000 && kWCon <= 10000) {
        descuento = 0.2;
    } else {
        descuento = 0.5;
    }

    // Aplicar el descuento al subtotal
    const descApply = subtotal * descuento;
    

    // Calcular el impuesto
    const impuesto = 0.16 * subtotal;

    // Calcular el total a pagar
    const total = subtotal + impuesto;

    // Aplicar el descuento de total a pagar
    const subtotalDesc = subtotal + impuesto - descApply;

    // Actualizar el objeto 'resultado'
    const params = {
        numRecibo,
        nombre,
        domicilio,
        servicio: tipoServicio,
        kWCon,
        costKw,
        subtotal,
        descuento: descApply,
        subtotalDesc,
        impuesto,
        total,
        isPost: true,
    };
    res.render('index', params);
});

export default { router }
